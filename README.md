Database deployed on the https://cloud.mongodb.com/ cloud.
Path to the database is determined in the application.properties file. You might use the current one.

To build project:

`gradle bootRun`

Endpoints:

- create:  /user/create, POST method
- update:  /user/update, POST method
- delete:  /user/delete, DELETE method
- get all: /user/getAll, GET method

User entity example:

```json
{
  "id": "ac23cacd23cf43vofa23",
  "email": "test_email@ukr.net",
  "name": "uname1"
}
```

Curl requests (quotes adaptation might be needed, dependently of operation system ):

---

Create:

```shell
curl --location --request POST 'http://localhost:8080/user/create' \
--header 'Content-Type: application/json' \
--data-raw '{
"email": "test_email@ukr.net",
"name": "uname1"
}'
```

Result: json of the user with id of the created user. Example:

```json
{
  "id": "ac23cacd23cf43vofa23",
  "email": "test_email@ukr.net",
  "name": "uname1"
}
```


---

Update:
```shell
curl --location --request POST 'http://localhost:8080/user/update' \
--header 'Content-Type: application/json' \
--data-raw '{
"email": "another_email@ukr.net",
"name": "uname1"
}'
```

Result: json of the updated user. Example:

```json
{
  "id": "ac23cacd23cf43vofa23",
  "email": "another_email@ukr.net",
  "name": "uname1"
}
```

---

Get all:
```shell
curl --location --request GET 'http://localhost:8080/user/getAll'
```

Result: json of the list of users. Example:

  ```json
[
    {
      "id": "ac23cacd23cf43vofa23",
      "email": "test_email@ukr.net",
      "name": "uname1"
    },
    {
      "id": "ac23cacd23cf43vofa24",
      "email": "test_email2@ukr.net",
      "name": "uname2"
    }
]
```

---

Delete:

```shell
curl --location --request DELETE 'http://localhost:8080/user/delete?id={$user_id}'
```

Result: json of the deleted user. Example:

```json
{
  "id": "ac23cacd23cf43vofa24",
  "email": "test_email2@ukr.net",
  "name": "uname2"
}
```
