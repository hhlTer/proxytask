package com.proxy.task.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The class represents the User entity.
 * Restrictions:
 *  email: should be not null, not empty and corresponds to the email format
 *  name: should be not null and not empty
 *
 * Json format:
 * {
 *     "id": "String value",
 *     "email": "String value",
 *     "name": "String value
 * }
 */
@Document("user")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class User {

    @Id
    private String id;

    @Indexed(unique = true)
    @Email(message = "Bot valid email structure")
    @NotNull
    private String email;
    @NotNull
    @NotBlank
    private String name;

}