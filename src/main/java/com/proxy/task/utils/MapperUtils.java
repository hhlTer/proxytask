package com.proxy.task.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proxy.task.exception.JsonSerializationException;
import org.springframework.boot.json.JsonParseException;

import java.io.IOException;

public class MapperUtils {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static String mapToJson(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new JsonSerializationException(e);
        }
    }
    public static <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, IOException {
        return objectMapper.readValue(json, clazz);
    }
}
