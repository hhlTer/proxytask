package com.proxy.task.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.proxy.task.utils.MapperUtils;
import com.proxy.task.model.User;
import com.proxy.task.service.UserInterface;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Controller serving the {@link User} entity with CRUD operation:
 *  - create:  /user/create, POST method
 *  - update:  /user/update, POST method
 *  - delete:  /user/delete, DELETE method
 *  - get all: /user/getAll, GET method
 */
@RestController
@RequestMapping("/user")
@Validated
public class UserController {

    private final UserInterface<User> userService;

    public UserController(
            @Autowired UserInterface<User> userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public ResponseEntity<User> create(
            @Valid @RequestBody User user){
        return ResponseEntity.ok(userService.create(user));
    }

    @GetMapping("/getAll")
    public ResponseEntity<Collection<User>> getAll(){
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping("/update")
    public ResponseEntity<User> update(
            @Valid @RequestBody User user
    ){
        return ResponseEntity.ok(userService.update(user));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> delete(
            @RequestParam("id") @NotNull @NotBlank String id
    ){
        User result = userService.delete(id);
        return ResponseEntity.ok("deleted: ".concat(MapperUtils.mapToJson(result)));
    }
}

