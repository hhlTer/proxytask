package com.proxy.task.exception;

import com.proxy.task.utils.MapperUtils;

/**
 * Threw in the case when a unique field will be duplicated. See {@link com.proxy.task.model.User#email}
 */
public class DuplicateEntityKeyException extends RuntimeException {
    public DuplicateEntityKeyException(String message, Object o) {
        super(message.concat(": ").concat(MapperUtils.mapToJson(o)));
    }
}
