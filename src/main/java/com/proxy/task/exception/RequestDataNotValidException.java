package com.proxy.task.exception;

import com.proxy.task.utils.MapperUtils;

/**
 * This Exception throws in the case unexpected (but valid) data were accepted.
 * Mainly determined while database data is processed, and we can't get wished data
 */
public class RequestDataNotValidException extends RuntimeException {
    public RequestDataNotValidException(String s, Object o) {
        super(s.concat(": ").concat(MapperUtils.mapToJson(o)));
    }
}
