package com.proxy.task.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Date;

/**
 * Handle exception and returns readable response in the case if expected error occurs.
 * Response format: {@link ErrorDetails}
 */
@ControllerAdvice
@Slf4j
public class ExceptionsHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({RequestDataNotValidException.class, DuplicateEntityKeyException.class, ConstraintViolationException.class})
    public ResponseEntity<Object> globalExceptionHandling(Exception exception, WebRequest request) {
        log.error(exception.getMessage());
        return new ResponseEntity<>(new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false)), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
