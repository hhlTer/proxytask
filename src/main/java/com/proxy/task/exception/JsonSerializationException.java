package com.proxy.task.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Threw in the case when entity can't be serialized in/from json.
 */
public class JsonSerializationException extends RuntimeException {
    public JsonSerializationException(JsonProcessingException e) {
        super(e);
    }
}
