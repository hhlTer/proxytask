package com.proxy.task.service;

import com.proxy.task.exception.DuplicateEntityKeyException;
import com.proxy.task.model.User;
import com.proxy.task.repository.UserRepository;
import com.proxy.task.exception.RequestDataNotValidException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@Slf4j
public class UserService implements UserInterface<User> {

    private final UserRepository userRepository;

    public UserService(
            @Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Create {@link com.proxy.task.model.User} in the databse
     * @param t     user entity
     * @return      created entity including created id
     */
    @Override
    public User create(User user) {
        try{
            log.info("Try to create user: {}", user);
            User result = userRepository.save(user);
            log.info("User {} created successfully", result);
            return result;
        } catch (DuplicateKeyException dke){
            throw new DuplicateEntityKeyException("Duplicate key for the creating user, (email have to be unique)", user);
        }
    }

    /**
     * Delete {@link com.proxy.task.model.User} from the database.
     * {@link User#id} used
     * @param id    user id
     * @return      deleted user entity
     */
    @Override
    public User delete(String id) {
        log.info("Try to get user before deleting with id: {}", id);
        User dbuser = getItemById(id);
        log.info("Try to delete user: {}", dbuser);
        userRepository.delete(dbuser);
        log.info("User was deleted successfully");
        return dbuser;
    }


    /**
     * Update {@link User}
     * @param user     user entity to updating
     * @return         user entity after updating, received from the DB
     */
    @Override
    public User update(User user) {
        log.info("Try to update user: {}", user);
        if(user.getId() == null || user.getId().isEmpty()){
            throw new RequestDataNotValidException("Id is null or empty", user);
        }
        User dbuser = getItemById(user.getId());
        User result = userRepository.save(user);
        log.info("User is successfully updated: {}", result);
        return result;
    }

    /**
     * Get all users from the DB
     * @return  Collection of the {@link User}
     */
    @Override
    public Collection<User> getAll() {
        log.info("Try to get all users from the DB");
        List<User> result = userRepository.findAll();
        log.info("Users are found. Amount of users: {}", result.size());
        return result;
    }

    private User getItemById(String id) {
        log.info("Try to found user by id: {}", id);
        User result = userRepository.findItemById(id);
        log.info("User is successfully received from the DB: {}", result);
        if(result == null){
            throw new RequestDataNotValidException("User with provided Id not found", id);
        }
        return result;
    }
}
