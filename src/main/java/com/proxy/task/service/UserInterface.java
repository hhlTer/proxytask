package com.proxy.task.service;


import java.util.Collection;

public interface UserInterface<T> {

    T create (T t);
    T delete(String id);
    T update(T t);
    Collection<T> getAll();
}
