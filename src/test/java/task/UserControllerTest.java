package task;

import com.proxy.task.model.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Comparator;

/**
 * This is an demonstration of the testing layer. Only CREATE test implemented
 * CRUD tests.
 *  - /user/create: implemented.
 *  - /getAll, /delete, /update: todo
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserControllerTest extends UserCrudApplicationTests{

    @BeforeAll
    public void init(){
        setUp();
        clearDb();
    }

    /**
     * Create {@link User} test.
     *  - the test:
     *    - create User
     *    - verify status code
     *    - verify returned fields:
     *      - id is exists and not empty
     *      - username and email aren't empty
     * @throws Exception
     */
    @Test
    public void CreateUserTest() throws Exception {
        final User expectedUser = new User();
        expectedUser.setEmail("asd@asd.com");
        expectedUser.setName("name");

        final String uri = "/user/create";
        final String body = mapToJson(expectedUser);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(body))
                .andReturn();
        String content = mvcResult.getResponse().getContentAsString();

        Assertions.assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        final User actualUser = mapFromJson(content, User.class);
        Comparator<User> isUserNameAndEmailEquals = (u1, u2) -> (u1.getEmail().equals(u2.getEmail()) && u1.getName().equals(u2.getName())) ? 0 : -1;
        Assertions.assertThat(actualUser)
                .as("Verify returned user fields: name, email")
                .usingComparator(isUserNameAndEmailEquals)
                .isEqualTo(expectedUser);

        Assertions.assertThat(actualUser.getId())
                .as("Verify the id is applied to the returned user")
                .isNotNull()
                .isNotEmpty();
    }
}
